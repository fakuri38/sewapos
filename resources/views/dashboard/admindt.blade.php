<!-- =========================================================
* Argon Dashboard PRO v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->
 <!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>SewaPos | Users</title>
  <!-- Favicon -->
  <link rel="icon" href="{{asset('css/img/brand/favicon.png')}}" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{asset('css/vendor/nucleo/css/nucleo.css')}}" type="text/css">
  <link rel="stylesheet" href="{{asset('css/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="{{asset('css/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{asset('css/css/argon.css?v=1.1.0')}}" type="text/css">
  <link rel="stylesheet" href="{{asset('css/css/bootstrap-multiselect.min.css')}}" type="text/css"/>
</head>

<body>
  <!-- Sidenav -->
  @include('dashboard.master')
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-red pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Admins</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Admins</a></li>
                </ol>
              </nav>
            </div>
        
            <div class="col-lg-6 col-5 text-right">
                
            <button type="button" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#exampleModal">
             New
            </button>
         
            <!--  <a href="{{route('admin.create')}}" class="btn btn-sm btn-neutral">New</a>-->
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="exampleModal"  id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Admin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <!--<div class="alert alert-danger print-error-msg" style="display:none">
                      <ul></ul>
                  </div>-->
                  <form class="needs-validation admin" novalidate>
                    @csrf
                        <div class="form-group mb-3">
                          <input type="text" class="form-control name-error" id="name" placeholder="Name" name="name"  autocomplete="cc-csc" required>
                          <div id="name-error-div" class="invalid-feedback ">
                          </div>
                        <!--    <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control name-error" placeholder="Name" id="name" type="text" name="name" autocomplete="cc-csc" >
                               
                            </div>
                            <div class="invalid-feedback">
                                  Required!
                            </div>-->
                        </div>
                        <div class="form-group mb-3">
                        <input type="text" class="form-control email-error" id="email" placeholder="E-mail" name="email"  autocomplete="cc-csc" required>
                          <div id="email-error-div" class="invalid-feedback">
                          </div>
                            <!--<div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control" placeholder="Email" id="email" type="email" name="email" autocomplete="cc-csc" >
                                <strong id="email-error"></strong>
                            </div>-->
                        </div>
                        <div class="form-group">
                        <input type="password" class="form-control password-error" id="password" placeholder="Password" name="password"  autocomplete="cc-csc" required>
                          <div id="password-error-div" class="invalid-feedback">
                          </div>
                        <!--    <div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input class="form-control" placeholder="Password" id="password" name="password" type="password" autocomplete="cc-csc">
                                <strong id="password-error"></strong>
                            </div>-->
                        </div>
                        <div class="form-group">
                        <input type="password" class="form-control password-error" id="confirm_password" placeholder="Re-enter Password" name="confirm_password"  autocomplete="cc-csc" required>
                          <div id="password-error-div" class="invalid-feedback">
                          </div>
                            <!--<div class="input-group input-group-merge input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input class="form-control" placeholder="Password" id="confirm_password" name="confirm_password" type="password" autocomplete="cc-csc">
                                <strong id="password-error"></strong>
                            </div>-->
                        </div>
                        <strong>Roles:</strong>
                        <div class="form-group">
                          <select id="roles" class="form-control" multiple="multiple" name="roles[]">
                          @foreach ( $roles as $roles )
                            <option value="{{$roles}}">{{$roles}}</option>
                            <!--<option value="tomatoes">Tomatoes</option>
                            <option value="mozarella">Mozzarella</option>
                            <option value="mushrooms">Mushrooms</option>
                            <option value="pepperoni">Pepperoni</option>
                            <option value="onions">Onions</option>-->
                          @endforeach 
                          </select>
                        </div>
                      </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button  class="btn btn-primary btn-submit">Save changes</button>
                        </div>
                    </form>
                </div>
              </div>
      </div>



      <div class="modal fade" id="DeleteArticleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are You Sure?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
              
                  </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary close-button" data-dismiss="modal">No</button>
                          <button type="submit" class="btn btn-primary" id="SubmitDeleteArticleForm">Yes</button>
                        </div>
               
                </div>
              </div>
      </div>
            
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatables" style="width:100%">
                <thead class="thead-light">
                  <tr>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Role</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @include('dashboard.dash-foot')
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{asset('css/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('css/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('css/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{asset('css/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{asset('css/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{asset('css/vendor/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
  <script src="{{asset('css/vendor/datatables.net-select/js/dataTables.select.min.js')}}"></script>
  <script src="{{asset('css/js/bootstrap-multiselect.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{asset('css/js/argon.js?v=1.1.0')}}"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="{{asset('css/js/demo.min.js')}}"></script>
  <script src="{{asset('css/custom/admindt.js')}}"></script>

  <script type="text/javascript">
       Url = {
          admin_index: "{{ route('admin.index') }}",
          admin_store: "{{ route('admin.store') }}"
       };
  </script>
  

<script type="text/javascript">
    $(document).ready(function() {
        $('#roles').multiselect();
    });
</script>

</body>

</html>