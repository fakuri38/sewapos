<!-- =========================================================
* Argon Dashboard PRO v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->
 <!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Juai | Roles</title>
  <!-- Favicon -->
  <link rel="icon" href="css/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{asset('css/vendor/nucleo/css/nucleo.css')}}" type="text/css">
  <link rel="stylesheet" href="{{asset('css/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="{{asset('css/vendor/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/vendor/quill/dist/quill.core.css')}}">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{asset('css/css/argon.css?v=1.1.0')}}" type="text/css">
</head>

<body>
  <!-- Sidenav -->
    @include('dashboard.master')
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-red pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Add Admin</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Admin</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Admin</li>
                </ol>
              </nav>
            </div>
         
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-lg-6">
          <div class="card-wrapper">
            <!-- Input groups -->
            <div class="card">
              <!-- Card header -->
              <!-- Card body -->
              <div class="card-body">
              <form method="POST" action="{{route('admin.store')}}">
              @csrf
                <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="confirm-password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                  </div>
                     <div class="form-group">
                     <strong>Role:</strong>
                        {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}           
                    </div>
                  </div>
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                 
                </div>
                <!-- /.card-body -->

               
              </form>
              </div>
            </div>
           
            <!-- Toggle buttons
            <div class="card">
              Card header 
              <div class="card-header">
                <h3 class="mb-0">Toggle buttons</h3>
              </div>-->
              <!-- Card body 
              <div class="card-body">
                <form>
                  <label class="custom-toggle">
                    <input type="checkbox">
                    <span class="custom-toggle-slider rounded-circle"></span>
                  </label>
                  <label class="custom-toggle">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                  <label class="custom-toggle custom-toggle-default">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                  <label class="custom-toggle custom-toggle-danger">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                  <label class="custom-toggle custom-toggle-warning">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                  <label class="custom-toggle custom-toggle-success">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                  <label class="custom-toggle custom-toggle-info">
                    <input type="checkbox" checked>
                    <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                  </label>
                </form>
              </div>
            </div>-->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{asset('css/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('css/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('css/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{asset('css/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{asset('css/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <!-- Optional JS -->
  <script src="{{asset('css/vendor/select2/dist/js/select2.min.js')}}"></script>
  <script src="{{asset('css/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('css/vendor/nouislider/distribute/nouislider.min.js')}}"></script>
  <script src="{{asset('css/vendor/quill/dist/quill.min.js')}}"></script>
  <script src="{{asset('css/vendor/dropzone/dist/min/dropzone.min.js')}}"></script>
  <script src="{{asset('css/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{asset('css/js/argon.js?v=1.1.0')}}"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="{{asset('css/js/demo.min.js')}}"></script>
</body>

</html>