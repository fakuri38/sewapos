<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admController;
use App\Http\Controllers\userController;
use App\Http\Controllers\roleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('admin', admController::class);
    Route::resource('user', userController::class);
    Route::resource('role', roleController::class);
    //Route::post('admin-submit', 'admController@store');
});