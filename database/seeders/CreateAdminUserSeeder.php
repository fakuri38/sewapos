<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use DB;
class CreateAdminUserSeeder extends Seeder
{
    use HasRoles;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin', 
            'email' => 'admin@pos.com.my',
            'password' => Hash::make('123456')
        ]);

        $path = public_path('users.sql');
        $sql = file_get_contents($path);
        DB::unprepared($sql);
    
        $role = Role::create(['name' => 'Admin']);
        $role2 = Role::create(['name' => 'User']);
        $datas = User::all();
        foreach ($datas as $data) {
            $data->assignRole([$role2->id]);
        }
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);
    }
}