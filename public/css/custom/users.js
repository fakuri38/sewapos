$(document).ready(function () {
    
    var table = $('#datatables').DataTable({
        
        processing: true,
        serverSide: true,
        ajax: Url.user_index,
        //scrollY:'50vh',

        
        responsive: true,
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            //{data: 'email_verified_at', name: 'email_verified_at',  orderable: true},
            {data: 'action', name: 'action'},
        ],
        dom: 'Bfrtip',
        /*buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],*/
        language: {
          paginate: {
            previous: "<i class='fas fa-angle-left'>",
            next: "<i class='fas fa-angle-right'>"
				}
			},
      
    });
    
    
  });