$(document).ready(function() {
    var table = $('#datatables').DataTable({
       
        serverSide: true,
        ajax: Url.admin_index,
       
        
        responsive: true,
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'role', name: 'role',  orderable: true},
            {data: 'action', name: 'action'},
        ],
        dom: 'Bfrtip',
        /*buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ],*/
        language: {
          paginate: {
            previous: "<i class='fas fa-angle-left'>",
            next: "<i class='fas fa-angle-right'>"
				}
			},
      
    });

    var deleteID;
        $('body').on('click', '#deleteAdmin', function(){
            deleteID = $(this).data('id');
        })
        $('#SubmitDeleteArticleForm').click(function(e) {
            e.preventDefault();
            var id = deleteID;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "admin/"+id,
                method: 'DELETE',
                success: function(result) {
                  $('#datatables').DataTable().ajax.reload();
                  $('#DeleteArticleModal').modal('hide');
                  $('.modal-backdrop').remove();
                }
            });
        });

        $(".btn-submit").click(function(e){
            e.preventDefault();
       
            var _token = $("input[name='_token']").val();
            var name = $("input[name='name']").val();
            var email = $("input[name='email']").val();
            var password = $("input[name='password']").val();
            var confirm_password = $("input[name='confirm_password']").val();
            var roles = $("#roles option:selected").toArray().map(roles => roles.text);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: Url.admin_store,
                type:'POST',
                data: {_token:_token, name:name, email:email, password:password, confirm_password:confirm_password, roles:roles},
                success: function(data) {
                    if($.isEmptyObject(data.error)){
                        //alert(data.success);
                        $('#datatables').DataTable().ajax.reload();
                        $('#exampleModal').modal('hide');
                        $(".admin")[0].reset();
                        $('.modal-backdrop').remove();
                    }else{
                      if(data.error['name']){
                        $( ".name-error" ).addClass("is-invalid")
                        $('#name-error-div').text(data.error['name']);
                        $('input[name="name"]').keypress(function() {
                        $( ".name-error" ).removeClass("is-invalid")
                        });
                      }
                      if(data.error['email']){
                        $( ".email-error" ).addClass("is-invalid")
                        $('#email-error-div').text(data.error['email']);
                        $('input[name="email"]').keypress(function() {
                        $( ".email-error" ).removeClass("is-invalid")
                        });
                      }
                      if(data.error['password']){
                        $( ".password-error" ).addClass("is-invalid")
                        $('#password-error-div').text(data.error['password']);
                        $('input[name="password"]').keypress(function() {
                        $( ".password-error" ).removeClass("is-invalid")
                        });
                      }
                      if(data.error['roles']){
                        $( ".role-error" ).addClass("is-invalid")
                        $('#role-error-div').text(data.error['roles']);
                        $('input[name="password"]').keypress(function() {
                        $( ".role-error" ).removeClass("is-invalid")
                        });
                      }
                      
                    }
                }
            });
       
        }); 
})