<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use DataTables;
  
class userController extends Controller
{
    use HasRoles;
    public function index(Request $request)
    {
        
        if ($request->ajax()) {
            //$data = User::role('User')->get();
            //$data = DB::table('users')->get();
  
                  //->select('id','name','email')
                  //->role('User')
                  //->get();
            $data = User::whereHas("roles", function($q){ $q->where("name", "User"); })->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('role', function($row){
                        foreach($row->getRoleNames() as $v){
                            return '<label class="badge badge-success">'.$v.'</label>';
                        }
                    })
                    ->addColumn('action', function($row){
     
                           $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['role','action'])
                    ->make(true);
        }
        return view('dashboard.datatables');
        
        //$data = User::role('Admin')->paginate(5);
        //$data->
        //$user->hasRoles('Admin');
        //$data = User::orderBy('id','ASC')->paginate(5);
       
    }

    public function getusers(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('id','email','name');
            return Datatables::of($data)
                    ->make(true);
        }
        return view('dashboard.admins.user-list');
        
        //$data = User::role('Admin')->paginate(5);
        //$data->
        //$user->hasRoles('Admin');
        //$data = User::orderBy('id','ASC')->paginate(5);
       
    }

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('dashboard.admins.create-admin',compact('roles'));
    }
    

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('admins.index')
                        ->with('success','User created successfully');
    }

  
}

