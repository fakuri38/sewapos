<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use DataTables;
use Validator;
  
class admController extends Controller
{
    use HasRoles;
    public function index(Request $request)
    {
        
        if ($request->ajax()) {
            $data = User::whereHas("roles", function($q) {
                $q->where("name", "!=", "User");
             })->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('role', function($row){
                        foreach($row->getRoleNames() as $v){
                            //$test = $row->roles->pluck('name')->toArray();
                            //$data = implode(',', $admin->roles->pluck('name')->toArray());
                            //'<label class="badge badge-success">'.$v.'</label>'; 
                            $btn = implode(', ', $row->roles->pluck('name')->toArray());  
                            return '<label class="badge badge-success">'.$btn.'</label>'; 
                            
                        }
                      
                    })
                    ->addColumn('action', function($row){
                        foreach($row->getRoleNames() as $v){
                            if($v == "Admin" AND $row->email == "admin@pos.com.my"){
                                return '<label class="badge badge-warning">uneditable</label>';
                            }else{
                                $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>
                                <a href="javascript:void(0)" data-id='.$row->id.' data-toggle="modal" data-target="#DeleteArticleModal" id="deleteAdmin" class="edit btn btn-warning btn-sm deleteAdmin">Delete</a>';
                                
    
                                return $btn;
                            }
                        }
                        
                    })
                    ->rawColumns(['role','action'])
                    ->make(true);
        }
        $roles = Role::pluck('name','name')->all();
        //return view('dashboard.admin.admin-create',compact('roles'));
        return view('dashboard.admindt',compact('roles'));
        
        //$data = User::role('Admin')->paginate(5);
        //$data->
        //$user->hasRoles('Admin');
        //$data = User::orderBy('id','ASC')->paginate(5);
       
    }

    public function getusers(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('id','email','name');
            return Datatables::of($data)
                    ->make(true);
        }
        return view('dashboard.admins.user-list');
        
        //$data = User::role('Admin')->paginate(5);
        //$data->
        //$user->hasRoles('Admin');
        //$data = User::orderBy('id','ASC')->paginate(5);
       
    }

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('dashboard.admin.admin-create',compact('roles'));
    }
    

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm_password',
            'roles' => 'required'
        ]);
        /*
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm_password',
            'roles' => 'required'
        ]);*/
    
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        
        if ($validator->passes()) {
            $user = User::create($input);
            $user->assignRole([$request->input('roles')]);
            /*return redirect()->route('admin.index')
                         ->with('success','User created successfully');*/
            return response()->json(['success'=>'Added new records.']);
        }
        return response()->json(['error'=>$validator->getMessageBag()->toArray()]);

        
        //return response()->json([ 'success'=> 'Form is successfully submitted!']);
        /*return redirect()->route('admin.index')
                         ->with('success','User created successfully');*/
    }

    public function destroy($id)
    {
        $roles = Role::pluck('name','name')->all();
        User::find($id)->delete();
        return view('dashboard.admindt',compact('roles'));
    }

  
}

