<?php

namespace App\Http\Controllers;

use App\Models\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use DataTables;
class roleController extends Controller
{
    public function index(Request $request){

        if ($request->ajax()) {
            $data = Role::orderBy('id','DESC')->get();
            
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                       if($row->name == "Admin" or $row->name == "HQ" or $row->name == "SCM" or $row->name == "User"){
                            return '<label class="badge badge-warning">uneditable</label>';
                       
                       }else{
                            $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
                            return $btn;
                       }
                   
                    })
                    ->rawColumns(['role','action'])
                    ->make(true);
        }
        return view('dashboard.roledt');
    }

    public function create(Request $request){
        $permission = Permission::get();
        return view('dashboard.role.role-create',compact('permission'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);
    
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
    
        return redirect()->route('role.index')
                        ->with('success','Role created successfully');

    }

    public function destroy(Request $request, $id){

        $input = $request->all();
        $role_name = $input['role_name'];

        $roles = DB::table('roles')
                 ->where('id', $id)
                 ->get();
       
        if( $roles == null){
            return redirect()->route('roles.create_roles')
            ->with('failed','no such roles');
        }else{

            DB::table('roles')
            ->where('id', $id)
            ->delete();

       
            return redirect()->route('roles.index')
            ->with('success','User and Roles deleted successfully');

        }

    }
}
